const Web3 = require('web3');
const jsonInterface = require('./jsonInterface.json')
const con_addr = "0xA1651195FD0d020045E8963f70707c6f60De0C4c";

export const connect = async () => {
    try {
        let web3 = new Web3(Web3.givenProvider);
        return new web3.eth.Contract(jsonInterface, con_addr);
    } catch (error) {
        console.error(error);
    }
}

export const getUserInfo = async (address) => {
    const contract = await connect();
    return await contract.methods.getCustomerInfo(address).call().then(res => {
        return res;
    });
}

export const getReferrer = async (referralCode) => {
    const contract = await connect();
    return await contract.methods.checkIfReferrerExist(referralCode).call()
}

export const invest = async (value, address) => {
    let web3 = new Web3(Web3.givenProvider);
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const ref = urlParams.get('ref');
    if (ref) {
        getRefAddress(ref).then(res => {
            return web3.eth.sendTransaction({
                from: address,
                to: con_addr,
                value: web3.utils.toWei(value, 'ether'),
                data: res,
            }).then(res => {
                return res;
            })
        })
    } else {
        return web3.eth.sendTransaction({
            from: address,
            to: con_addr,
            value: web3.utils.toWei(value, 'ether'),
        }).then(res => {
            return res;
        })
    }
}

export const withdraw = async (address, fakeWithdraw) => {
    const contract = await connect();

    return await contract.methods.widthdraw(fakeWithdraw).send({
        from: address,
    }).then(res => {
        return res;
    })
}

export const claim = async (address, fakeWithdraw) => {
    const contract = await connect();
    return await contract.methods.calculateClaim(address, fakeWithdraw).call().then(res => {
        return res;
    })
}

export const getRefAddress = async (id) => {
    const contract = await connect();
    return await contract.methods.checkIfReferrerExist(id).call().then(res => {
        return res;
    })
}

export const checkUserStatus = async (address) => {
    const contract = await connect();
    return await contract.methods.getUserRegistrationStatus(address).call()
    .then(res => {
        console.log(address);
        return res;
    })
    .catch(function(reason) {
        return false
     });
}



