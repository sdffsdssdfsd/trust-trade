import React from "react";
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {HomeLayout, DashboardLayout} from "./Layouts";
import Dashboard from "./components/Dashboard/Dashboard";
import Homepage from "./components/Homepage/Hompage";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import './App.css';
import SnackbarProvider from 'react-simple-snackbar'


function RouteWrapper({component: Component, layout: Layout, ...rest}) {
    return (
        <Route {...rest} render={(props) =>
            <Layout {...props}>
                <Component {...props} />
            </Layout>
        }/>
    );
}

function App() {
    return (
        <BrowserRouter>
            <SnackbarProvider>
                <Switch>
                    <RouteWrapper exact path="/" component={Homepage} layout={HomeLayout}/>
                    <RouteWrapper exact path="/dashboard" component={Dashboard} layout={DashboardLayout}/>
                </Switch>
            </SnackbarProvider>
        </BrowserRouter>
    );
}

export default App;
