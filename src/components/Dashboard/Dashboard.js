import React, {useState} from 'react';
import Loader from "react-loader-spinner";
import Clipboard from 'react-clipboard.js';
import {invest, withdraw, claim} from "../../helper";
import {useSnackbar} from 'react-simple-snackbar'
import Web3 from "web3";

export default function Dashboard(props) {
    const {userData} = props;
    const [loading, setLoading] = useState(false);
    const [claimAmount, setClaimAmount] = useState('');
    const [fakeWithdraw, setFakeWithdraw] = useState('')
    const [openSnackbar] = useSnackbar({position: 'bottom-left'});

    const plans = [
        {
            id: '1101',
            title: 'VIP',
            amount: '0.2',
            dividends: '2.5',
            days: '7',
            referral: '10'
        },
        {
            id: '1102',
            title: 'GOLD',
            amount: '0.15',
            dividends: '4',
            days: '15',
            referral: '8'
        },
        {
            id: '1103',
            title: 'SILVER',
            amount: '0.1',
            dividends: '6',
            days: '20',
            referral: '6'
        },
        {
            id: '1104',
            title: 'BRONZE',
            amount: '0.08',
            dividends: '5',
            days: '30',
            referral: '5'
        }
    ]

    const handleFakeWithdrawChange = e => {
        setFakeWithdraw(e.target.value);
    }

    const handleWithdraw = () => {
        setLoading(true)
        withdraw(userData.address, fakeWithdraw).then(res => {
            console.log(res);
            openSnackbar(res);
            setLoading(false)
        }).catch(err => {
            console.log(err);
            openSnackbar(err.message);
            setLoading(false)
        })
    }

    const handleGetClaim = () => {
        claim(userData.address, fakeWithdraw).then(res => {
            let claim = Web3.utils.fromWei(res[0], 'ether')
            console.log(res[0]);
            openSnackbar(claim);
            setClaimAmount(claim);
            setLoading(false)
        }).catch(err => {
            console.log(err);
            openSnackbar(err.message);
            setLoading(false)
        })
    }

    return (
        <div className="user-wraper" style={{margin: '110px 0'}}>
            <div className="container">
                <div className="d-flex">
                    <div className="user-content">
                        <div className="user-panel">
                            <div className="row p-4">
                                <div className="col-md-6">
                                    <div className="tile-item tile-primary">
                                        <div className="tile-bubbles"/>
                                        <h6 className="tile-title">YOUR CLAIM</h6>
                                        <h1 className="tile-info">
                                            {userData.balance
                                                ? userData.balance
                                                : <Loader
                                                    type="ThreeDots"
                                                    color="#00BFFF"
                                                    height={50}
                                                    width={50}
                                                    className={"d-inline-block"}
                                                />
                                            }
                                            <strong className="ml-2">ETH</strong>
                                        </h1>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="tile-item tile-primary d-flex flex-column justify-content-center">
                                        <div className="tile-bubbles"/>
                                        <h5 className="tile-title">YOUR INVESTMENT INCOME:
                                            {userData.address
                                                ?
                                                <span className="text-white w-100 address"> 0.4 ETH</span>
                                                : <Loader
                                                    type="ThreeDots"
                                                    color="#00BFFF"
                                                    height={50}
                                                    width={50}
                                                    className={"d-inline-block"}
                                                />
                                            }
                                        </h5>

                                        <h6 className="tile-title">YOUR REFERRAL INCOME:
                                            {userData.address
                                                ?
                                                <span className="text-white address"> 0.5 ETH</span>
                                                : <Loader
                                                    type="ThreeDots"
                                                    color="#00BFFF"
                                                    height={50}
                                                    width={50}
                                                    className={"d-inline-block"}
                                                />
                                            }
                                        </h6>

                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="tile-item tile-light">
                                        <h6 className="tile-title">YOUR WALLET ADDRESS</h6>
                                        {userData.address
                                            ?
                                            <h1 className="text-black-50 d-inline-block w-75 address">{userData.address}</h1>
                                            : <Loader
                                                type="ThreeDots"
                                                color="#00BFFF"
                                                height={50}
                                                width={50}
                                                className={"d-inline-block"}
                                            />
                                        }
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="tile-item tile-light">
                                        <div className="tile-bubbles"/>
                                        <h6 className="tile-title">YOUR INVESTMENT</h6>
                                        <h1 className="text-black-50">{userData.planId !== "" && userData.planId > 0 ? plans.find(e => e.id === userData.planId).amount : '0'} ETH</h1>
                                    </div>
                                </div>
                            </div>
                            <section className="row p-4">
                                <div className="col-md-12">
                                    <div
                                        className="tile-item tile-primary w-100 d-flex justify-content-between align-items-center">
                                        <div className="tile-bubbles"/>
                                        <div>
                                            <h1>Withdraw</h1>
                                            <h2>Total Claim:
                                                <strong className="ml-2">
                                                    {claimAmount}
                                                </strong>
                                                <strong className="ml-2">ETH</strong>
                                            </h2>

                                        </div>
                                        <form className="w-75" id="withdraw-form">
                                            <div className="form-group" style={{height: '50px'}}>
                                                <div className="input-group-append h-100">
                                                    <button className="btn btn-success" type="button"
                                                            onClick={handleWithdraw}>
                                                        {loading ? <Loader
                                                            type="Puff"
                                                            color="#ffffff"
                                                            height={30}
                                                            width={30}
                                                            className={"d-inline-block"}
                                                        /> : 'withdraw'}
                                                    </button>
                                                    <button className="btn btn-warning" type="button"
                                                            onClick={handleGetClaim}>
                                                        {loading ? <Loader
                                                            type="Puff"
                                                            color="#ffffff"
                                                            height={30}
                                                            width={30}
                                                            className={"d-inline-block"}
                                                        /> : 'get claim'}
                                                    </button>
                                                </div>
                                                <input type="text"
                                                       className="form-control w-100 h-100 border border-danger"
                                                       placeholder="fakeWithdraw" onChange={handleFakeWithdrawChange}/>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <div className="row justify-content-center">
                                    <div className="col-10">
                                        <h6>My unique referral link</h6>
                                        <div className="refferal-info">
                                            <span className="refferal-copy-feedback copy-feedback">
                                                Copied to Clipboard
                                            </span>
                                            <em className="fas fa-link"></em>
                                            <input type="text" className="refferal-address"
                                                   value={`${window.location.origin}?ref=${userData.refId}`}
                                                   disabled/>
                                            <Clipboard className="refferal-copy copy-clipboard"
                                                       data-clipboard-text={`${window.location.origin}?ref=${userData.refId}`}>
                                                <em className="ti ti-files"></em>
                                            </Clipboard>
                                        </div>
                                        <div className="gaps-2x"></div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}
