import React from 'react';
import Loader from 'react-loader-spinner'


export default function DashboardHeader(props) {
    const {userData} = props;

    return (
        <div className="topbar">
            <div className="topbar-md d-lg-none">
                <div className="container">
                    <div className="d-flex align-items-center justify-content-between">
                        <a href="#" className="toggle-nav">
                            <div className="toggle-icon">
                                <span className="toggle-line"></span>
                                <span className="toggle-line"></span>
                                <span className="toggle-line"></span>
                                <span className="toggle-line"></span>
                            </div>
                        </a>
                        <div className="site-logo">
                            <a href="/" className="site-brand">
                                <img src="images/logo.png" alt="logo" srcSet="images/logo2x.png 2x"/>
                            </a>
                        </div>

                        <div className="dropdown topbar-action-item topbar-action-user">
                            <a href="#" data-toggle="dropdown">
                                <img className="icon" src={require("../Dashboard/images/user-thumb-sm.png")}
                                     alt="thumb"/>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right">
                                <div className="user-dropdown">
                                    <div className="user-dropdown-head">
                                        <h6 className="user-dropdown-name">Stefan Harary <span>(IXIA1A105)</span></h6>
                                        <span className="user-dropdown-email">useremail@example.com</span>
                                    </div>
                                    <div className="user-dropdown-balance">
                                        <h6>ICO TOKEN BALANCE</h6>
                                        <h3>120,000,000 IC0X</h3>
                                        <ul>
                                            <li>1.240 BTC</li>
                                            <li>19.043 ETH</li>
                                            <li>6,500.13 USD</li>
                                        </ul>
                                    </div>
                                    <ul className="user-dropdown-btns btn-grp guttar-10px">
                                        <li><a href="#" className="btn btn-xs btn-warning">Confirm Email</a></li>
                                        <li><a href="kyc.html" className="btn btn-xs btn-warning">KYC Pending</a></li>
                                    </ul>
                                    <div className="gaps-1x"></div>
                                    <ul className="user-dropdown-links">
                                        <li><a href="account.html"><i className="ti ti-id-badge"></i>My Profile</a></li>
                                        <li><a href="security.html"><i className="ti ti-lock"></i>Security</a></li>
                                        <li><a href="activity.html"><i className="ti ti-eye"></i>Activity</a></li>
                                    </ul>
                                    <ul className="user-dropdown-links">
                                        <li><a href="login.html"><i className="ti ti-power-off"></i>Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="d-lg-flex align-items-center justify-content-between">
                    <div className="topbar-lg d-none d-lg-block">
                        <div className="site-logo">
                            <a href="/" className="site-brand">
                                <img src="images/logo.png" alt="logo" srcSet="images/logo2x.png 2x"/>
                            </a>
                        </div>
                    </div>

                    <div className="topbar-action d-none d-lg-block">
                        <ul className="topbar-action-list">
                            <li className="topbar-action-item topbar-action-link">
                                <a href="/"><em className="ti ti-home"></em> Go to main site</a>
                            </li>
                            <li className="dropdown topbar-action-item topbar-action-user">
                                <a href="#" data-toggle="dropdown"> <img className="icon" src="images/user-thumb-sm.png"
                                                                         alt="thumb"/> </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <div className="user-dropdown">
                                        <div className="user-dropdown-head">
                                            <h6 className="user-dropdown-name">Your Wallets Address</h6>
                                            <span className="user-dropdown-email">
                                                {userData.address
                                                    ? userData.address
                                                    : <Loader
                                                        type="ThreeDots"
                                                        color="#00BFFF"
                                                        height={50}
                                                        width={50}
                                                    />
                                                }
                                            </span>
                                        </div>
                                        <div className="user-dropdown-balance">
                                            <h6>BALANCE</h6>
                                            <h3>
                                                {userData.balance
                                                    ? userData.balance
                                                    : <Loader
                                                        type="ThreeDots"
                                                        color="#00BFFF"
                                                        height={50}
                                                        width={50}
                                                        className={"d-inline-block"}
                                                    />
                                                }
                                                <strong className="ml-2">ETH</strong>
                                            </h3>
                                        </div>
                                        <ul className="user-dropdown-links">
                                            <li><a href="/"><i className="ti ti-power-off"></i>Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
