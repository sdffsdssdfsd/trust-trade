import React, {useState} from 'react';
import Loader from "react-loader-spinner";
import {checkUserStatus, getUserInfo, invest} from "../../helper";
import {useSnackbar} from "react-simple-snackbar";
import {SHA3} from "sha3";

export default function Homepage(props) {
    const [loading, setLoading] = useState(false);
    const [address, setAddress] = useState('')
    const [openSnackbar] = useSnackbar({
        position: 'bottom-left',
    });
    const [userData, setUserData] = useState(
        {
            connected: false,
            address: '',
            balance: '',
            totalWithdraw: '',
            lastWithdrawal: '',
            investTime: '',
            numReferral: '',
            totalRefPayment: '',
            planId: '',
            claim: '',
        }
    )

    const plans = [
        {
            id: '1101',
            title: 'VIP',
            amount: '0.2',
            dividends: '2.5',
            days: '7',
            referral: '10'
        },
        {
            id: '1102',
            title: 'GOLD',
            amount: '0.15',
            dividends: '4',
            days: '15',
            referral: '8'
        },
        {
            id: '1103',
            title: 'SILVER',
            amount: '0.1',
            dividends: '6',
            days: '20',
            referral: '6'
        },
        {
            id: '1104',
            title: 'BRONZE',
            amount: '0.08',
            dividends: '5',
            days: '30',
            referral: '5'
        }
    ]

    const handleInvest = async e => {
        let address = localStorage.getItem("address");
        if (address === 'metaMask') {
            await window.ethereum.request({method: 'eth_requestAccounts'}).then(res => {
                address = res[0];
            });
        }
        await checkUserStatus(address).then(res => {
            if (res != false) {
                alert('You have invested already!');
            } else {
                setLoading(true);
                openSnackbar("Transaction Sent.Please Confirm", 500000);
                const id = e.target.id;
                const value = e.target.value;
                const referrer = userData.numReferral !== '0' ? userData.numReferral : '0x543b2b8d53BAD4e19a5D1682f88b145D78627528';
                invest(value, userData.address, id, referrer).then(res => {
                    console.log(res);
                    openSnackbar(res);
                    setLoading(false);
                }).catch(err => {
                    console.log(err);
                    openSnackbar(err.message);
                    setLoading(false);
                });
            }
        }).catch(err => {
            console.log(err);
            alert('You are not connected!');
        })
    }

    const isAddress = (address) => {
        if (!/^(0x)?[0-9a-f]{40}$/i.test(address)) {
            // check if it has the basic requirements of an address
            return false;
        } else if (/^(0x)?[0-9a-f]{40}$/.test(address) || /^(0x)?[0-9A-F]{40}$/.test(address)) {
            // If it's all small caps or all all caps, return true
            return true;
        } else {
            // Otherwise check each case
            return isChecksumAddress(address);
        }
    };

    const isChecksumAddress = (address) => {
        // Check each case
        address = address.replace('0x', '');
        const hash = new SHA3();
        const addressHash = hash.update(address.toLowerCase());
        for (let i = 0; i < 40; i++) {
            // the nth letter should be uppercase if the nth digit of casemap is 1
            if ((parseInt(addressHash[i], 16) > 7 && address[i].toUpperCase() !== address[i]) || (parseInt(addressHash[i], 16) <= 7 && address[i].toLowerCase() !== address[i])) {
                return false;
            }
        }
        return true;
    };

    const handleSubmit = () => {
        if (!address.length) {
            alert('Please enter a valid ETH address');
            return;
        } else {
            if (!isAddress(address)) {
                alert('Your wallet address is not valid');
                return;
            }
        }
        localStorage.setItem('address', address);
        window.location.reload();
    }

    const handleInputChange = (e) => {
        setAddress(e.target.value);
    }

    const handleMetaMask = async () => {
        localStorage.setItem('address', 'metaMask');
        const accounts = await window.ethereum.request({method: 'eth_requestAccounts'});
        if (userData.totalWithdraw === '') {
            getUserInfo(accounts[0]).then(async res => {
                if (typeof res[0] !== "undefined") {
                    setUserData(
                        {
                            ...userData,
                            connected: true,
                            address: accounts[0],
                            totalWithdraw: res[0],
                            lastWithdrawal: res[1],
                            investTime: res[2],
                            numReferral: res[3],
                            totalRefPayment: res[4],
                            planId: res[5],
                        }
                    )
                }

            });
        }
    }

    return (
        <main className="nk-pages">
            <section className="section section-r bg-white pb-0 ov-h">
                <div className="container">
                    <div className="row p-3" id="why">
                        <div className="col-12 mb-5">
                            <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                                data-delay=".1">INVEST NOW</h6>
                            <h2 className="title animated" data-animate="fadeInUp" data-delay=".2">Our Plans For
                                You</h2>
                        </div>
                        {typeof plans[0].amount !== "undefined" && plans.map(data => {
                            return (
                                <div className="col-lg-3 col-md-3">
                                    <div
                                        className="feature feature-s8 feature-s8-alt feature-center card card-full-lg card-md animated bg-light"
                                        data-animate="fadeInUp" data-delay=".4">
                                        <div className="feature-icon feature-icon-lg feature-icon-lg-s2">
                                            <img src="images/icons/icon-bg-a.png" alt="feature"/>
                                            <span>{data.title}</span>
                                        </div>
                                        <div className="feature-text feature-text-s8">
                                            <div className="bg-white rounded">{data.dividends} %</div>
                                            <ul>
                                                <li>Dividends every {data.days} days</li>
                                                <li>Forever</li>
                                                <li>{data.amount} ETH investment</li>
                                                <li>{data.referral}% From referrals</li>
                                            </ul>
                                            {userData.connected
                                                ? <button onClick={handleInvest}
                                                          id={data.id}
                                                          value={data.amount}
                                                          className="mt-4 w-100 btn btn-outline-primary d-flex align-items-center justify-content-center invest-btn">
                                                        <span className="mr-2" style={{color: '#304073'}}>
                                                            {loading
                                                                ? <Loader
                                                                    type="Puff"
                                                                    color="#00BFFF"
                                                                    height={30}
                                                                    width={30}
                                                                    className={"d-inline-block"}
                                                                />
                                                                : 'INVEST'}</span>
                                                </button>
                                                : <button type="button" className="btn btn-primary" data-toggle="modal"
                                                          data-target="#connect-modal">
                                                    connect
                                                </button>
                                            }
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </section>
            <section className="section bg-light-grad" id="about">
                <div className="container">
                    <div className="nk-block nk-block-features-s2">
                        <div className="row align-items-center gutter-vr-30px">
                            <div className="col-md-6">
                                <div className="gfx mx-auto mx-lg-0 animated" data-animate="fadeInUp" data-delay=".1">
                                    <img src="images/gfx/gfx-r.png" alt="gfx"/>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="nk-block-text text-center text-md-left">
                                    <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                                        data-delay=".2">What is ICO Crypto</h6>
                                    <h2 className="title animated" data-animate="fadeInUp" data-delay=".3">We’ve built a
                                        platform <br/>to buy and sell shares.</h2>
                                    <p className="lead animated" data-animate="fadeInUp" data-delay=".4">ICO Crypto is a
                                        platform for the future of funding that powering dat for the new equity
                                        blockchain</p>
                                    <p className="animated" data-animate="fadeInUp" data-delay=".5">While existing
                                        solutions offer to solve just one problem at a time, our team is up to build a
                                        secure, useful, &amp; easy-to-use product based on private blockchain. It will
                                        include easy cryptocurrency payments integration, and even a digital arbitration
                                        system.</p>
                                    <p className="animated" data-animate="fadeInUp" data-delay=".6">At the end, Our aims
                                        to integrate all companies, employees, and business assets into a unified
                                        blockchain ecosystem, which will make business truly efficient, transparent, and
                                        reliable.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="section bg-white">
                <div className="container">
                    <div className="nk-block nk-block-lg nk-block-features-s2">
                        <div className="row align-items-center flex-row-reverse gutter-vr-30px">
                            <div className="col-md-6">
                                <div className="gfx animated" data-animate="fadeInUp" data-delay=".1">
                                    <img src="images/gfx/gfx-s-light.png" alt="gfx"/>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="nk-block-text">
                                    <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                                        data-delay=".2">Power of Blockchain</h6>
                                    <h2 className="title animated" data-animate="fadeInUp" data-delay=".3">Tokenization
                                        Benefits</h2>
                                    <p className="animated" data-animate="fadeInUp" data-delay=".4">Ut enim ad minim
                                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat.</p>
                                    <p className="animated" data-animate="fadeInUp" data-delay=".5">Cryptocurrencies are
                                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        laudantium, totam rem.</p>
                                    <p className="animated" data-animate="fadeInUp" data-delay=".6">Nemo enim ipsam
                                        voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                        consequuntur magni dolores eos qui ratione voluptatem sequi nesciun quis
                                        nostrut.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="nk-block nk-block-lg nk-block-features-s2">
                        <div className="row align-items-center gutter-vr-30px">
                            <div className="col-md-6">
                                <div className="gfx animated" data-animate="fadeInUp" data-delay=".1">
                                    <img src="images/gfx/gfx-t-light.png" alt="gfx"/>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="nk-block-text">
                                    <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                                        data-delay=".2">Global Community</h6>
                                    <h2 className="title animated" data-animate="fadeInUp" data-delay=".3">The ICO
                                        Community</h2>
                                    <p className="animated" data-animate="fadeInUp" data-delay=".4">Tempor incididunt ut
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                        exercitation ullamco laboris nisi ut aliquip.</p>
                                    <p className="animated" data-animate="fadeInUp" data-delay=".5">Perspiciatis unde
                                        omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam
                                        rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
                                        beatae vitae dicta sunt. Nemo enim ipsam voluptatem quia voluptas sit aspernatur
                                        aut odit.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="nk-block nk-block-features-s2">
                        <div className="row align-items-center flex-row-reverse gutter-vr-30px">
                            <div className="col-md-6">
                                <div className="gfx animated" data-animate="fadeInUp" data-delay=".1">
                                    <img src="images/gfx/gfx-u-light.png" alt="gfx"/>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="nk-block-text">
                                    <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                                        data-delay=".2">Ensure A Safe</h6>
                                    <h2 className="title animated" data-animate="fadeInUp" data-delay=".3">Adaptive
                                        Smart Contracts</h2>
                                    <p className="animated" data-animate="fadeInUp" data-delay=".4">Mostly due to sed do
                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat.</p>
                                    <p className="animated" data-animate="fadeInUp" data-delay=".5">Cryptocurrencies are
                                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                        veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                    <p className="animated" data-animate="fadeInUp" data-delay=".6">Nemo enim ipsam
                                        voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                        consequuntur magni dolores eos qui ratione voluptatem sequi nesciun quis
                                        nostrut.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="section bg-light" id="why">
                <div className="background-shape bs-reverse"></div>
                <div className="container">
                    <div className="section-head section-head-s9 wide-sm">
                        <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                            data-delay=".1">How it Work</h6>
                        <h2 className="title animated" data-animate="fadeInUp" data-delay=".2">Best Features</h2>
                        <p className="animated" data-animate="fadeInUp" data-delay=".3">The ICO Crypto Team combines a
                            passion for esports, industry experise &amp; proven record in finance, development,
                            marketing.</p>
                    </div>
                    <div className="nk-block">
                        <div className="row gutter-vr-40px justify-content-center">
                            <div className="col-lg-4 col-md-4">
                                <div
                                    className="feature feature-s8 feature-s8-alt feature-center card card-full-lg card-md animated"
                                    data-animate="fadeInUp" data-delay=".4">
                                    <div className="feature-icon feature-icon-lg feature-icon-lg-s2">
                                        <img src="images/icons/icon-h.png" alt="feature"/>
                                    </div>
                                    <div className="feature-text feature-text-s8">
                                        <h4 className="title title-sm title-thin title-s5">
                                            <span>Ultra Fast &amp; Secure </span><span>Instant Private Transaction</span>
                                        </h4>
                                        <p>The Smart Asset Blockcjain os starter &amp; more open with lorem Ipsum is
                                            simply text of the crypto.</p>
                                        <a href="#" className="link link-primary link-feature-s1"><em
                                            className="link-icon icon-circle icon-circle-md ti ti-arrow-right"></em></a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-4">
                                <div
                                    className="feature feature-s8 feature-s8-alt feature-center card card-full-lg card-md animated"
                                    data-animate="fadeInUp" data-delay=".5">
                                    <div className="feature-icon feature-icon-lg feature-icon-lg-s2">
                                        <img src="images/icons/icon-i.png" alt="feature"/>
                                    </div>
                                    <div className="feature-text feature-text-s8">
                                        <h4 className="title title-sm title-thin title-s5"><span>Highly Scalable </span><span>Limitless Applications</span>
                                        </h4>
                                        <p>The Smart Asset Blockcjain os starter &amp; more open with lorem Ipsum is
                                            simply text of the crypto.</p>
                                        <a href="#" className="link link-primary link-feature-s1"><em
                                            className="link-icon icon-circle icon-circle-md ti ti-arrow-right"></em></a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-4">
                                <div
                                    className="feature feature-s8 feature-s8-alt feature-center card card-full-lg card-md animated"
                                    data-animate="fadeInUp" data-delay=".6">
                                    <div className="feature-icon feature-icon-lg feature-icon-lg-s2">
                                        <img src="images/icons/icon-j.png" alt="feature"/>
                                    </div>
                                    <div className="feature-text feature-text-s8">
                                        <h4 className="title title-sm title-thin title-s5">
                                            <span>Reliable &amp; Low Cost </span><span>Instant Private Transaction</span>
                                        </h4>
                                        <p>The Smart Asset Blockcjain os starter &amp; more open with lorem Ipsum is
                                            simply text of the crypto.</p>
                                        <a href="#" className="link link-primary link-feature-s1"><em
                                            className="link-icon icon-circle icon-circle-md ti ti-arrow-right"></em></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center pdt-r">
                            <ul className="btn-grp animated" data-animate="fadeInUp" data-delay=".7">
                                <li><a href="#" className="btn btn-md btn-grad">Get a Wallet</a></li>
                                <li><a href="#" className="btn btn-md btn-outline btn-grad on-bg-light">Download
                                    Whitepaper</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section className="section bg-white" id="benifits">
                <div className="container">
                    <div className="section-head section-head-s9 wide-sm">
                        <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                            data-delay=".1">ICO Crypto Feature</h6>
                        <h2 className="title animated" data-animate="fadeInUp" data-delay=".2">Ecosystem key
                            features</h2>
                        <p className="animated" data-animate="fadeInUp" data-delay=".3">The ICO Crypto Team combines a
                            passion for esports, industry experise &amp; proven record in finance, development,
                            marketing.</p>
                    </div>
                    <div className="nk-block nk-block-features">
                        <div className="row gutter-vr-60px gutter-100px">
                            <div className="col-lg-6">
                                <div className="feature feature-s12 animated" data-animate="fadeInUp" data-delay=".4">
                                    <div className="feature-icon feature-icon-lg-s1 m-lg-0">
                                        <img src="images/icons/icon-d-light.png" alt="feature"/>
                                    </div>
                                    <div className="feature-text feature-text-s2">
                                        <p>ICO Crypto makes <strong> you the sole owner of a
                                            secure </strong> decentralize registry for your collection an products</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="feature feature-s12 animated" data-animate="fadeInUp" data-delay=".5">
                                    <div className="feature-icon feature-icon-lg-s1 m-lg-0">
                                        <img src="images/icons/icon-e-light.png" alt="feature"/>
                                    </div>
                                    <div className="feature-text feature-text-s2">
                                        <p>The registry is a <strong>tamper-proof</strong> that uses <strong>dolor sit
                                            amet,</strong> conse ctetur sed eiusmod tempor incidid labore Lorem
                                            consectetur adipiscing.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="feature feature-s12 animated" data-animate="fadeInUp" data-delay=".6">
                                    <div className="feature-icon feature-icon-lg-s1 m-lg-0">
                                        <img src="images/icons/icon-f-light.png" alt="feature"/>
                                    </div>
                                    <div className="feature-text feature-text-s2">
                                        <p>Provide your customer with a <strong>lorem ipsum dolor sit amet, conse ctetur
                                            sed</strong> eiusmod tempor incidid labore Lorem ipsum dolor.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="feature feature-s12 animated" data-animate="fadeInUp" data-delay=".7">
                                    <div className="feature-icon feature-icon-lg-s1 m-lg-0">
                                        <img src="images/icons/icon-g-light.png" alt="feature"/>
                                    </div>
                                    <div className="feature-text feature-text-s2">
                                        <p>ICO Crypto the prowess of blockchain <strong>labore et dolore</strong> dolor
                                            sit amet, conse <strong>Ctetur sed</strong> eiusmod tempor labore dolor
                                            adipiscing.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="section bg-light">
                <div className="container">
                    <div className="section-head section-head-s9 wide-md">
                        <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                            data-delay=".1">ICO Market</h6>
                        <h2 className="title animated" data-animate="fadeInUp" data-delay=".2">Introducing the ICO
                            Marketplace</h2>
                        <div className="wide-sm">
                            <p className="animated" data-animate="fadeInUp" data-delay=".3">Blockchain-powered
                                marketplace which connects buyer and Lorem ipsum dolor sit amet, consectetur
                                adipiscing.</p>
                        </div>
                    </div>
                    <div className="nk-block nk-block-text-wrap">
                        <div className="row align-items-center gutter-vr-30px">
                            <div className="col-lg-6">
                                <div className="nk-block-img animated" data-animate="fadeInUp" data-delay=".4">
                                    <img src="images/jasmine/laptop-mobile-a.png" alt="app"/>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="nk-block-text">
                                    <ul className="list list-check list-check-s3">
                                        <li className="animated" data-animate="fadeInUp" data-delay=".5">The Platform
                                            will earn premium libero tempore, cum soluta nobis keep their digital coins
                                            est eligendi optio.
                                        </li>
                                        <li className="animated" data-animate="fadeInUp" data-delay=".55">At aut
                                            reiciendis voluptatibus maiores alias conse quatur aut perferendis.
                                            Contributor payouts perfectly simple and easy
                                        </li>
                                        <li className="animated" data-animate="fadeInUp" data-delay=".6">Ut enim ad
                                            minim veniam, quis nostrud exerc itation identification is stored in an
                                            encrypted ullamco laboris nisi.
                                        </li>
                                        <li className="animated" data-animate="fadeInUp" data-delay=".65">Owners of
                                            cryptocurrency keep their digital coins in ICO digital wallet. A
                                            coin-holder’s identification is stored in an encrypted address that they
                                            have control.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="section bg-white" id="token">
                <div className="container">
                    <div className="section-head section-head-s9 wide-md">
                        <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                            data-delay=".1">Token</h6>
                        <h2 className="title animated" data-animate="fadeInUp" data-delay=".2">Token Sale</h2>
                        <div className="wide-sm">
                            <p className="animated" data-animate="fadeInUp" data-delay=".3">The tokens will be available
                                for transfer thar is rorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div className="nk-block nk-block-token">
                        <div className="row gutter-vr-30px">
                            <div className="col-lg-7">
                                <div className="row">
                                    <div className="col-sm-4">
                                        <div className="token-stage text-center animated" data-animate="fadeInUp"
                                             data-delay=".4">
                                            <div className="token-stage-title token-stage-pre">Pre Sale</div>
                                            <div className="token-stage-date">
                                                <h6>3 July 2018</h6>
                                                <span>10 Days</span>
                                            </div>
                                            <div className="token-stage-info">
                                                <span className="token-stage-bonus">30% Bonus</span>
                                                <span className="token-stage-cap">Soft Cap</span>
                                                <span className="token-stage-amount">$2M</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-4">
                                        <div className="token-stage text-center animated" data-animate="fadeInUp"
                                             data-delay=".5">
                                            <div className="token-stage-title token-stage-one">Sale Stage 1</div>
                                            <div className="token-stage-date">
                                                <h6>15 August 2018</h6>
                                                <span>15 Days</span>
                                            </div>
                                            <div className="token-stage-info">
                                                <span className="token-stage-bonus">30% Bonus</span>
                                                <span className="token-stage-cap">Soft Cap</span>
                                                <span className="token-stage-amount">$2M</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-4">
                                        <div className="token-stage text-center animated" data-animate="fadeInUp"
                                             data-delay=".6">
                                            <div className="token-stage-title">Sale Stage 2t</div>
                                            <div className="token-stage-date">
                                                <h6>28 October 2018</h6>
                                                <span>10 Days</span>
                                            </div>
                                            <div className="token-stage-info">
                                                <span className="token-stage-bonus">10% Bonus</span>
                                                <span className="token-stage-cap">Hard Cap</span>
                                                <span className="token-stage-amount">$3M</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="token-action-box text-center animated" data-animate="fadeInUp"
                                     data-delay=".7">
                                    <div className="token-action-title">Join Our <br/> Pre-Sale List</div>
                                    <div className="token-action-date"><strong>Pre-Sale Start at</strong> 03 July 2018
                                    </div>
                                    <div className="token-action-btn">
                                        <a href="#" className="btn btn-lg btn-grad">Signup &amp; Join</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-5">
                                <table className="table table-token table-token-s1 animated" data-animate="fadeInUp"
                                       data-delay=".8">
                                    <tbody>
                                    <tr>
                                        <td className="table-head table-head-s1">Token Symbol</td>
                                        <td className="table-des table-des-s1">ICOX</td>
                                    </tr>
                                    <tr>
                                        <td className="table-head table-head-s1">Token Sale Start</td>
                                        <td className="table-des table-des-s1">3 July 2018</td>
                                    </tr>
                                    <tr>
                                        <td className="table-head table-head-s1">Token Sale End</td>
                                        <td className="table-des table-des-s1">28 October 2018</td>
                                    </tr>
                                    <tr>
                                        <td className="table-head table-head-s1">Tokens for sale</td>
                                        <td className="table-des table-des-s1">100,500,000</td>
                                    </tr>
                                    <tr>
                                        <td className="table-head table-head-s1">Specifications</td>
                                        <td className="table-des table-des-s1">ICOX token</td>
                                    </tr>
                                    <tr>
                                        <td className="table-head table-head-s1">Max circulating supply</td>
                                        <td className="table-des table-des-s1">175,500,000</td>
                                    </tr>
                                    <tr>
                                        <td className="table-head table-head-s1">Sale duration</td>
                                        <td className="table-des table-des-s1">10 days</td>
                                    </tr>
                                    <tr>
                                        <td className="table-head table-head-s1">Sale duration</td>
                                        <td className="table-des table-des-s1">10 days</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="nk-block">
                        <div className="row">
                            <div className="col-lg-12">
                                <h3 className="sub-heading-s2 animated" data-animate="fadeInUp" data-delay=".1">Token
                                    Allocation</h3>
                                <div className="token-bar-chart animated" data-animate="fadeInUp" data-delay=".2">
                                    <div className="token-bar-item tbic1" data-percent="8">
                                        <div className="token-bar-txt">
                                            <span>8%</span>
                                            <span>Bounty</span>
                                        </div>
                                    </div>
                                    <div className="token-bar-item tbic2" data-percent="12">
                                        <div className="token-bar-txt">
                                            <span>12%</span>
                                            <span>Presale</span>
                                        </div>
                                    </div>
                                    <div className="token-bar-item tbic3" data-percent="20">
                                        <div className="token-bar-txt">
                                            <span>20%</span>
                                            <span>Team &amp; Advisor</span>
                                        </div>
                                    </div>
                                    <div className="token-bar-item tbic4" data-percent="25">
                                        <div className="token-bar-txt">
                                            <span>25%</span>
                                            <span>Reserve Fund</span>
                                        </div>
                                    </div>
                                    <div className="token-bar-item tbic5" data-percent="35">
                                        <div className="token-bar-txt">
                                            <span>35%</span>
                                            <span>Reserve Fund</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mgt-l">
                            <div className="col-lg-12">
                                <h3 className="sub-heading-s2 animated" data-animate="fadeInUp"
                                    data-delay=".3">Operating Allocation</h3>
                                <div className="token-bar-chart animated" data-animate="fadeInUp" data-delay=".4">
                                    <div className="token-bar-item tbic5" data-percent="13">
                                        <div className="token-bar-txt">
                                            <span>13%</span>
                                            <span>Admin &amp; Ops</span>
                                        </div>
                                    </div>
                                    <div className="token-bar-item tbic4" data-percent="17">
                                        <div className="token-bar-txt">
                                            <span>17%</span>
                                            <span>Legal &amp; Advisory</span>
                                        </div>
                                    </div>
                                    <div className="token-bar-item tbic3" data-percent="25">
                                        <div className="token-bar-txt">
                                            <span>25%</span>
                                            <span>Marketing &amp; Sales</span>
                                        </div>
                                    </div>
                                    <div className="token-bar-item tbic2" data-percent="45">
                                        <div className="token-bar-txt">
                                            <span>45%</span>
                                            <span>Business Dev &amp; Ops</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="section bg-light">
                <div className="container">
                    <div className="section-head section-head-s9 wide-md">
                        <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                            data-delay=".1">Download Documents</h6>
                        <h2 className="title animated" data-animate="fadeInUp" data-delay=".2">Read Our Documents</h2>
                        <div className="wide-sm">
                            <p className="animated" data-animate="fadeInUp" data-delay=".3">Here is our full documents
                                that help you to understand about us. And lorem ipsum dolor sit amet, consectetur
                                adipiscing elit.</p>
                        </div>
                    </div>
                    <div className="nk-block nk-block-features">
                        <div className="row gutter-vr-30px">
                            <div className="col-xl-3 col-sm-6 col-mb-10">
                                <div className="doc doc-s2 bg-white animated" data-animate="fadeInUp" data-delay=".4">
                                    <div className="doc-photo no-hover">
                                        <img src="images/docs/alt-sm-a.png" alt="doc"/>
                                    </div>
                                    <div className="doc-text">
                                        <h6 className="doc-title title-xs-alt">Whitepaper</h6>
                                        <ul className="btn-grp gutter-10px">
                                            <li><a className="btn btn-outline btn-xxs btn-auto btn-light"
                                                   href="#">ENG</a></li>
                                            <li><a className="btn btn-outline btn-xxs btn-auto btn-light"
                                                   href="#">RUS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-sm-6 col-mb-10">
                                <div className="doc doc-s2 bg-white animated" data-animate="fadeInUp" data-delay=".5">
                                    <div className="doc-photo no-hover">
                                        <img src="images/docs/alt-sm-b.png" alt="doc"/>
                                    </div>
                                    <div className="doc-text">
                                        <h6 className="doc-title title-xs-alt">OnePager</h6>
                                        <ul className="btn-grp gutter-10px">
                                            <li><a className="btn btn-outline btn-xxs btn-auto btn-light"
                                                   href="#">ENG</a></li>
                                            <li><a className="btn btn-outline btn-xxs btn-auto btn-light"
                                                   href="#">RUS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-sm-6 col-mb-10">
                                <div className="doc doc-s2 bg-white animated" data-animate="fadeInUp" data-delay=".6">
                                    <div className="doc-photo no-hover">
                                        <img src="images/docs/alt-sm-c.png" alt="doc"/>
                                    </div>
                                    <div className="doc-text">
                                        <h6 className="doc-title title-xs-alt">Privacy Policy</h6>
                                        <ul className="btn-grp gutter-10px">
                                            <li><a className="btn btn-outline btn-xxs btn-auto btn-light"
                                                   href="#">ENG</a></li>
                                            <li><a className="btn btn-outline btn-xxs btn-auto btn-light"
                                                   href="#">RUS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-sm-6 col-mb-10">
                                <div className="doc doc-s2 bg-white animated" data-animate="fadeInUp" data-delay=".7">
                                    <div className="doc-photo no-hover">
                                        <img src="images/docs/alt-sm-d.png" alt="doc"/>
                                    </div>
                                    <div className="doc-text">
                                        <h6 className="doc-title title-xs-alt">Terms of Sale</h6>
                                        <ul className="btn-grp gutter-10px">
                                            <li><a className="btn btn-outline btn-xxs btn-auto btn-light"
                                                   href="#">ENG</a></li>
                                            <li><a className="btn btn-outline btn-xxs btn-auto btn-light"
                                                   href="#">RUS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="section bg-white" id="roadmap">
                <div className="container">
                    <div className="section-head section-head-s9 wide-md">
                        <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                            data-delay=".1">Timeline</h6>
                        <h2 className="title animated" data-animate="fadeInUp" data-delay=".2">Road Map</h2>
                        <div className="wide-sm">
                            <p className="animated" data-animate="fadeInUp" data-delay=".3">Our team working hardly to
                                make archive lorem ipsum dolor sit amet, consectetur amet, consectetur adipiscing
                                elit.</p>
                        </div>
                    </div>
                    <div className="nk-block nk-block-left">
                        <div className="roadmap-all mgb-m50 animated" data-animate="fadeInUp" data-delay=".4">
                            <div
                                className="roadmap-wrap roadmap-wrap-done roadmap-wrap-s1 roadmap-wrap-s1-alt mb-0 ml-0">
                                <div className="row no-gutters">
                                    <div className="col-lg">
                                        <div className="roadmap roadmap-s1  roadmap-s1-alt roadmap-done text-lg-center">
                                            <div className="roadmap-step roadmap-step-s1">
                                                <div className="roadmap-head roadmap-head-s1">
                                                    <span className="roadmap-time roadmap-time-s1">2017 Q1</span>
                                                    <span className="roadmap-title roadmap-title-s1">Concept</span>
                                                </div>
                                                <ul className="roadmap-step-list roadmap-step-list-s1">
                                                    <li>Concept Generation</li>
                                                    <li>Team Assemble</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg">
                                        <div className="roadmap roadmap-s1  roadmap-s1-alt roadmap-done text-lg-center">
                                            <div className="roadmap-step roadmap-step-s1">
                                                <div className="roadmap-head roadmap-head-s1">
                                                    <span className="roadmap-time roadmap-time-s1">2017 Q2</span>
                                                    <span className="roadmap-title roadmap-title-s1">Research</span>
                                                </div>
                                                <ul className="roadmap-step-list roadmap-step-list-s1">
                                                    <li>Proving the concept can work</li>
                                                    <li>Strategic Plan</li>
                                                    <li>White paper conpletion</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg">
                                        <div className="roadmap roadmap-s1  roadmap-s1-alt roadmap-done text-lg-center">
                                            <div className="roadmap-step roadmap-step-s1">
                                                <div className="roadmap-head roadmap-head-s1">
                                                    <span className="roadmap-time roadmap-time-s1">2018 Q1</span>
                                                    <span className="roadmap-title roadmap-title-s1">Design</span>
                                                </div>
                                                <ul className="roadmap-step-list roadmap-step-list-s1">
                                                    <li>Platform design and technical demonstration</li>
                                                    <li>Building the MVP</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg">
                                        <div className="roadmap roadmap-s1  roadmap-s1-alt roadmap-done text-lg-center">
                                            <div className="roadmap-step roadmap-step-s1">
                                                <div className="roadmap-head roadmap-head-s1">
                                                    <span className="roadmap-time roadmap-time-s1">2018 Q2</span>
                                                    <span className="roadmap-title roadmap-title-s1">Pre-Sale</span>
                                                </div>
                                                <ul className="roadmap-step-list roadmap-step-list-s1">
                                                    <li>Public financing &amp; Seed funding raised</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="roadmap-wrap roadmap-wrap-s1 roadmap-wrap-s1-alt mb-0 ml-0">
                                <div className="row flex-row-reverse no-gutters">
                                    <div className="col-lg">
                                        <div
                                            className="roadmap roadmap-current roadmap-s1  roadmap-s1-alt text-lg-center">
                                            <div className="roadmap-step roadmap-step-s1">
                                                <div className="roadmap-head roadmap-head-s1">
                                                    <span className="roadmap-time roadmap-time-s1">2018 Q3</span>
                                                    <span
                                                        className="roadmap-title roadmap-title-s1">App Beta Test</span>
                                                </div>
                                                <ul className="roadmap-step-list roadmap-step-list-s1">
                                                    <li>Private closed beta</li>
                                                    <li>Open beta launched to public and improvement the app</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg">
                                        <div className="roadmap roadmap-s1  roadmap-s1-alt text-lg-center">
                                            <div className="roadmap-step roadmap-step-s1">
                                                <div className="roadmap-head roadmap-head-s1">
                                                    <span className="roadmap-time roadmap-time-s1">2018 Q4</span>
                                                    <span className="roadmap-title roadmap-title-s1">Token Sale</span>
                                                </div>
                                                <ul className="roadmap-step-list roadmap-step-list-s1">
                                                    <li>ICO Press Tour</li>
                                                    <li>Open global sales of ICOblock token</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg">
                                        <div className="roadmap roadmap-s1  roadmap-s1-alt text-lg-center">
                                            <div className="roadmap-step roadmap-step-s1">
                                                <div className="roadmap-head roadmap-head-s1">
                                                    <span className="roadmap-time roadmap-time-s1">2018 Q1</span>
                                                    <span className="roadmap-title roadmap-title-s1">Alpha Test</span>
                                                </div>
                                                <ul className="roadmap-step-list roadmap-step-list-s1">
                                                    <li>In-house testing of functional</li>
                                                    <li>Prototype published and linked to Ethereum blockchain with
                                                        real-time scanning
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="roadmap-wrap roadmap-wrap-s1 roadmap-wrap-s1-alt mb-lg-0 ml-0">
                                <div className="row no-gutters">
                                    <div className="col-lg">
                                        <div className="roadmap roadmap-s1  roadmap-s1-alt text-lg-center">
                                            <div className="roadmap-step roadmap-step-s1">
                                                <div className="roadmap-head roadmap-head-s1">
                                                    <span className="roadmap-time roadmap-time-s1">2019 Q2</span>
                                                    <span className="roadmap-title roadmap-title-s1">Crowdfunding Integration</span>
                                                </div>
                                                <ul className="roadmap-step-list roadmap-step-list-s1">
                                                    <li>Smart contracts support creators</li>
                                                    <li>Ethereum tokens support</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg">
                                        <div className="roadmap roadmap-s1  roadmap-s1-alt text-lg-center">
                                            <div className="roadmap-step roadmap-step-s1">
                                                <div className="roadmap-head roadmap-head-s1">
                                                    <span className="roadmap-time roadmap-time-s1">2019 Q3</span>
                                                    <span
                                                        className="roadmap-title roadmap-title-s1">Community Benefits</span>
                                                </div>
                                                <ul className="roadmap-step-list roadmap-step-list-s1">
                                                    <li>Establishing global user base</li>
                                                    <li>US start retailer selection</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg">
                                        <div className="roadmap roadmap-s1  roadmap-s1-alt text-lg-center">
                                            <div className="roadmap-step roadmap-step-s1">
                                                <div className="roadmap-head roadmap-head-s1">
                                                    <span className="roadmap-time roadmap-time-s1">2019 Q3</span>
                                                    <span
                                                        className="roadmap-title roadmap-title-s1">Hardware things</span>
                                                </div>
                                                <ul className="roadmap-step-list roadmap-step-list-s1">
                                                    <li>Integration of third party controllers</li>
                                                    <li>Marketplace cooperative module</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg">
                                        <div className="roadmap roadmap-s1 roadmap-s1-alt text-lg-center">
                                            <div className="roadmap-step roadmap-step-s1">
                                                <div className="roadmap-head roadmap-head-s1">
                                                    <span className="roadmap-time roadmap-time-s1">2020 Q1</span>
                                                    <span
                                                        className="roadmap-title roadmap-title-s1">More Operational</span>
                                                </div>
                                                <ul className="roadmap-step-list roadmap-step-list-s1">
                                                    <li>Integration with Private Chains, More Coin in Wallet</li>
                                                    <li>New services offered by members or business</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="section bg-light-alt" id="team">
                <div className="background-shape bs-right"></div>
                <div className="container">
                    <div className="section-head section-head-s9 wide-md">
                        <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                            data-delay=".1">MEET THE TEAM</h6>
                        <h2 className="title animated" data-animate="fadeInUp" data-delay=".2">Executive team</h2>
                        <div className="wide-sm">
                            <p className="animated" data-animate="fadeInUp" data-delay=".3">The ICO Crypto Team combines
                                a passion for esports, industry experise &amp; proven record in finance, development,
                                marketing &amp; licensing.</p>
                        </div>
                    </div>
                    <div className="nk-block nk-block-left nk-block-team-list team-list">
                        <div className="row justify-content-start">
                            <div className="col-lg-3 col-sm-6">
                                <div className="team team-s4 round bg-white ml-0 animated" data-animate="fadeInUp"
                                     data-delay=".4">
                                    <div className="team-photo team-photo-s1 round-full">
                                        <img src="images/team/sq-a.jpg" alt="team" className="round-full"/>
                                    </div>
                                    <h5 className="team-name">Waylon Dalton</h5>
                                    <span className="team-position tc-primary">CEO &amp; Lead Blockchain</span>
                                    <div className="team-desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incide.</p>
                                    </div>
                                    <ul className="team-social team-social-s2">
                                        <li><a href="#"><em className="fab fa-linkedin-in"></em></a></li>
                                        <li><a href="#"><em className="fab fa-facebook-f"></em></a></li>
                                        <li><a href="#"><em className="fab fa-twitter"></em></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-3 col-sm-6">
                                <div className="team team-s4 round bg-white ml-0 animated" data-animate="fadeInUp"
                                     data-delay=".5">
                                    <div className="team-photo team-photo-s1 round-full">
                                        <img src="images/team/sq-b.jpg" alt="team" className="round-full"/>
                                    </div>
                                    <h5 className="team-name">Stefan Harary</h5>
                                    <span className="team-position tc-primary">CEO &amp; Lead Blockchain</span>
                                    <div className="team-desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incide.</p>
                                    </div>
                                    <ul className="team-social team-social-s2">
                                        <li><a href="#"><em className="fab fa-linkedin-in"></em></a></li>
                                        <li><a href="#"><em className="fab fa-facebook-f"></em></a></li>
                                        <li><a href="#"><em className="fab fa-twitter"></em></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-3 col-sm-6">
                                <div className="team team-s4 round bg-white ml-0 animated" data-animate="fadeInUp"
                                     data-delay=".6">
                                    <div className="team-photo team-photo-s1 round-full">
                                        <img src="images/team/sq-c.jpg" alt="team" className="round-full"/>
                                    </div>
                                    <h5 className="team-name">Moises Teare</h5>
                                    <span className="team-position tc-primary">CEO &amp; Lead Blockchain</span>
                                    <div className="team-desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incide.</p>
                                    </div>
                                    <ul className="team-social team-social-s2">
                                        <li><a href="#"><em className="fab fa-linkedin-in"></em></a></li>
                                        <li><a href="#"><em className="fab fa-facebook-f"></em></a></li>
                                        <li><a href="#"><em className="fab fa-twitter"></em></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-3 col-sm-6">
                                <div className="team team-s4 round bg-white ml-0 animated" data-animate="fadeInUp"
                                     data-delay=".7">
                                    <div className="team-photo team-photo-s1 round-full">
                                        <img src="images/team/sq-d.jpg" alt="team" className="round-full"/>
                                    </div>
                                    <h5 className="team-name">Gabriel Bernal</h5>
                                    <span className="team-position tc-primary">CEO &amp; Lead Blockchain</span>
                                    <div className="team-desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incide.</p>
                                    </div>
                                    <ul className="team-social team-social-s2">
                                        <li><a href="#"><em className="fab fa-linkedin-in"></em></a></li>
                                        <li><a href="#"><em className="fab fa-facebook-f"></em></a></li>
                                        <li><a href="#"><em className="fab fa-twitter"></em></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="nk-block nk-block-left nk-block-team-list">
                        <div className="section-head section-head-sm">
                            <h2 className="title-lg-2 animated" data-animate="fadeInUp" data-delay=".8">ADVISORS</h2>
                        </div>
                        <div className="row justify-content-start">
                            <div className="col-lg-3 col-sm-6">
                                <div className="team team-s4 round bg-white ml-0 animated" data-animate="fadeInUp"
                                     data-delay=".9">
                                    <div className="team-photo team-photo-s1 round-full">
                                        <img src="images/team/sq-a.jpg" alt="team" className="round-full"/>
                                    </div>
                                    <h5 className="team-name">Waylon Dalton</h5>
                                    <span className="team-position tc-primary">CEO &amp; Lead Blockchain</span>
                                    <div className="team-desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incide.</p>
                                    </div>
                                    <ul className="team-social team-social-s2">
                                        <li><a href="#"><em className="fab fa-linkedin-in"></em></a></li>
                                        <li><a href="#"><em className="fab fa-facebook-f"></em></a></li>
                                        <li><a href="#"><em className="fab fa-twitter"></em></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-3 col-sm-6">
                                <div className="team team-s4 round bg-white ml-0 animated" data-animate="fadeInUp"
                                     data-delay="1">
                                    <div className="team-photo team-photo-s1 round-full">
                                        <img src="images/team/sq-b.jpg" alt="team" className="round-full"/>
                                    </div>
                                    <h5 className="team-name">Stefan Harary</h5>
                                    <span className="team-position tc-primary">CEO &amp; Lead Blockchain</span>
                                    <div className="team-desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incide.</p>
                                    </div>
                                    <ul className="team-social team-social-s2">
                                        <li><a href="#"><em className="fab fa-linkedin-in"></em></a></li>
                                        <li><a href="#"><em className="fab fa-facebook-f"></em></a></li>
                                        <li><a href="#"><em className="fab fa-twitter"></em></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-3 col-sm-6">
                                <div className="team team-s4 round bg-white ml-0 animated" data-animate="fadeInUp"
                                     data-delay="1.1">
                                    <div className="team-photo team-photo-s1 round-full">
                                        <img src="images/team/sq-c.jpg" alt="team" className="round-full"/>
                                    </div>
                                    <h5 className="team-name">Moises Teare</h5>
                                    <span className="team-position tc-primary">CEO &amp; Lead Blockchain</span>
                                    <div className="team-desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incide.</p>
                                    </div>
                                    <ul className="team-social team-social-s2">
                                        <li><a href="#"><em className="fab fa-linkedin-in"></em></a></li>
                                        <li><a href="#"><em className="fab fa-facebook-f"></em></a></li>
                                        <li><a href="#"><em className="fab fa-twitter"></em></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="section bg-white">
                <div className="container">
                    <div className="section-head section-head-s9 wide-md">
                        <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                            data-delay=".1">Partners</h6>
                        <h2 className="title animated" data-animate="fadeInUp" data-delay=".2">Supported By</h2>
                    </div>
                    <div className="nk-block block-partners">
                        <ul className="partner-list partner-list-left partner-list-s3 flex-wrap">
                            <li className="partner-logo partner-logo-s2 animated" data-animate="fadeInUp"
                                data-delay=".3">
                                <img src="images/partners/a-xs-alt.png" alt="partner"/>
                            </li>
                            <li className="partner-logo partner-logo-s2 animated" data-animate="fadeInUp"
                                data-delay=".35">
                                <img src="images/partners/b-xs-alt.png" alt="partner"/>
                            </li>
                            <li className="partner-logo partner-logo-s2 animated" data-animate="fadeInUp"
                                data-delay=".4">
                                <img src="images/partners/c-xs-alt.png" alt="partner"/>
                            </li>
                            <li className="partner-logo partner-logo-s2 animated" data-animate="fadeInUp"
                                data-delay=".45">
                                <img src="images/partners/d-xs-alt.png" alt="partner"/>
                            </li>
                            <li className="partner-logo partner-logo-s2 animated" data-animate="fadeInUp"
                                data-delay=".5">
                                <img src="images/partners/e-xs-alt.png" alt="partner"/>
                            </li>
                            <li className="partner-logo partner-logo-s2 animated" data-animate="fadeInUp"
                                data-delay=".55">
                                <img src="images/partners/a-xs-alt.png" alt="partner"/>
                            </li>
                            <li className="partner-logo partner-logo-s2 animated" data-animate="fadeInUp"
                                data-delay=".6">
                                <img src="images/partners/a-sm-alt.png" alt="partner"/>
                            </li>
                            <li className="partner-logo partner-logo-s2 animated" data-animate="fadeInUp"
                                data-delay=".65">
                                <img src="images/partners/b-sm-alt.png" alt="partner"/>
                            </li>
                            <li className="partner-logo partner-logo-s2 animated" data-animate="fadeInUp"
                                data-delay=".7">
                                <img src="images/partners/c-sm-alt.png" alt="partner"/>
                            </li>
                            <li className="partner-logo partner-logo-s2 animated" data-animate="fadeInUp"
                                data-delay=".75">
                                <img src="images/partners/a-sm-alt.png" alt="partner"/>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
            <section className="section bg-light" id="faqs">
                <div className="container">
                    <div className="section-head section-head-s9 wide-md">
                        <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                            data-delay=".1">FAQ</h6>
                        <h2 className="title animated" data-animate="fadeInUp" data-delay=".2">Frequently asked
                            questions</h2>
                        <div className="wide-sm">
                            <p className="animated" data-animate="fadeInUp" data-delay=".3">Below we’ve provided a bit
                                of ICO, ICO Token, cryptocurrencies, and few others. If you have any other questions,
                                please get in touch using the contact form below.</p>
                        </div>
                    </div>
                    <div className="nk-block">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-md-12">
                                <ul className="nav tab-nav tab-nav-btn pdb-r justify-content-start animated"
                                    data-animate="fadeInUp" data-delay=".4">
                                    <li><a className="active" data-toggle="tab" href="#general-questions-13">General</a>
                                    </li>
                                    <li><a data-toggle="tab" href="#ico-questions-13">Pre-ICO &amp; ICO</a></li>
                                    <li><a data-toggle="tab" href="#tokens-sales-13">Token</a></li>
                                </ul>
                            </div>
                            <div className="col-lg-8">
                                <div className="tab-content animated" data-animate="fadeInUp" data-delay=".5">
                                    <div className="tab-pane fade show active" id="general-questions-13">
                                        <div className="accordion accordion-faq" id="faq-47">
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm"
                                                    data-toggle="collapse" data-target="#faq-47-1">
                                                    What is ICO Crypto?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-47-1" className="collapse show" data-parent="#faq-47">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm collapsed"
                                                    data-toggle="collapse" data-target="#faq-47-2">
                                                    What cryptocurrencies can I use to purchase?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-47-2" className="collapse" data-parent="#faq-47">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm collapsed"
                                                    data-toggle="collapse" data-target="#faq-47-3">
                                                    How can I participate in the ICO Token sale?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-47-3" className="collapse" data-parent="#faq-47">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm collapsed"
                                                    data-toggle="collapse" data-target="#faq-47-4">
                                                    How do I benefit from the ICO Token?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-47-4" className="collapse" data-parent="#faq-47">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tab-pane fade" id="ico-questions-13">
                                        <div className="accordion accordion-faq" id="faq-48">
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm"
                                                    data-toggle="collapse" data-target="#faq-48-1">
                                                    Which of us ever undertakes laborious?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-48-1" className="collapse show" data-parent="#faq-48">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm collapsed"
                                                    data-toggle="collapse" data-target="#faq-48-2">
                                                    Who do not know how to pursue rationally?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-48-2" className="collapse" data-parent="#faq-48">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm collapsed"
                                                    data-toggle="collapse" data-target="#faq-48-3">
                                                    Their separate existence is a myth?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-48-3" className="collapse" data-parent="#faq-48">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm collapsed"
                                                    data-toggle="collapse" data-target="#faq-48-4">
                                                    Pityful a rethoric question ran over her cheek?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-48-4" className="collapse" data-parent="#faq-48">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tab-pane fade" id="tokens-sales-13">
                                        <div className="accordion accordion-faq" id="faq-49">
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm"
                                                    data-toggle="collapse" data-target="#faq-49-1">
                                                    When she reached the first hills?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-49-1" className="collapse show" data-parent="#faq-49">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm collapsed"
                                                    data-toggle="collapse" data-target="#faq-49-2">
                                                    Big Oxmox advised her not to do?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-49-2" className="collapse" data-parent="#faq-49">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm collapsed"
                                                    data-toggle="collapse" data-target="#faq-49-3">
                                                    Which roasted parts of sentences fly into your mouth?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-49-3" className="collapse" data-parent="#faq-49">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-item accordion-item-s2 bg-white">
                                                <h5 className="accordion-title accordion-title-sm collapsed"
                                                    data-toggle="collapse" data-target="#faq-49-4">
                                                    Vokalia and Consonantia, there live?
                                                    <span className="accordion-icon accordion-icon-s2"></span>
                                                </h5>
                                                <div id="faq-49-4" className="collapse" data-parent="#faq-49">
                                                    <div className="accordion-content">
                                                        <p>Once ICO period is launched, You can purchased Token with
                                                            Etherum, Bitcoin or Litecoin. You can also tempor incididunt
                                                            ut labore et dolore magna aliqua sed do eiusmod eaque
                                                            ipsa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="nk-block-img mt-4 mt-lg-0 animated" data-animate="fadeInUp"
                                     data-delay=".6">
                                    <img src="images/gfx/gfx-p.png" alt="lungwort"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="section section-contact bg-white ov-h" id="contact">
                <div className="container">
                    <div className="nk-block block-contact">
                        <div className="row justify-content-center gutter-vr-30px">
                            <div className="col-lg-3">
                                <div className="section-head section-head-sm section-head-s9 text-center text-lg-left">
                                    <h6 className="title title-xs title-s1 tc-primary animated" data-animate="fadeInUp"
                                        data-delay=".1">Contact</h6>
                                    <h2 className="title animated" data-animate="fadeInUp" data-delay=".2">Get In
                                        Touch</h2>
                                    <div className="class">
                                        <p className="animated" data-animate="fadeInUp" data-delay=".3">Any question?
                                            Reach out to us and we’ll get back to you shortly.</p>
                                    </div>
                                </div>
                                <div className="d-flex flex-column justify-content-between h-100">
                                    <ul className="contact-list contact-list-s2">
                                        <li className="animated" data-animate="fadeInUp" data-delay=".4">
                                            <em className="contact-icon contact-icon-s2 fas fa-phone"></em>
                                            <div className="contact-text">
                                                <span>+44 0123 4567</span>
                                            </div>
                                        </li>
                                        <li className="animated" data-animate="fadeInUp" data-delay=".5">
                                            <em className="contact-icon contact-icon-s2 fas fa-envelope"></em>
                                            <div className="contact-text">
                                                <span>info@company.com</span>
                                            </div>
                                        </li>
                                        <li className="animated" data-animate="fadeInUp" data-delay=".6">
                                            <em className="contact-icon contact-icon-s2 fas fa-paper-plane"></em>
                                            <div className="contact-text">
                                                <span>Join us on Telegram</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-4 offset-lg-1">
                                <div className="gap-6x d-none d-lg-block"></div>
                                <div className="gap-4x d-none d-lg-block"></div>
                                <form id="contact-form-01" className="contact-form nk-form-submit"
                                      action="form/contact.php" method="post">
                                    <div className="field-item field-item-s2 animated" data-animate="fadeInUp"
                                         data-delay=".7">
                                        <input name="contact-name" type="text" className="input-bordered required"
                                               placeholder="Your Name"/>
                                    </div>
                                    <div className="field-item field-item-s2 animated" data-animate="fadeInUp"
                                         data-delay=".8">
                                        <input name="contact-email" type="email"
                                               className="input-bordered required email" placeholder="Your Email"/>
                                    </div>
                                    <div className="field-item field-item-s2 animated" data-animate="fadeInUp"
                                         data-delay=".9">
                                        <textarea name="contact-message"
                                                  className="input-bordered input-textarea required"
                                                  placeholder="Your Message"></textarea>
                                    </div>
                                    <input type="text" className="d-none" name="form-anti-honeypot" value=""/>
                                    <div className="row">
                                        <div className="col-sm-12 animated" data-animate="fadeInUp" data-delay="1">
                                            <button type="submit" className="btn btn-s2 btn-md btn-grad">Submit
                                            </button>
                                        </div>
                                        <div className="col-sm-12">
                                            <div className="form-results"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="col-lg-4 align-self-center">
                                <div className="nk-block-img animated" data-animate="fadeInUp" data-delay="1.1">
                                    <img src="images/gfx/gfx-q.png" alt="lungwort"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div className="modal fade" id="connect-modal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div className="modal-content bg-white">
                        <div className="modal-header">
                            <span className="modal-title" id="exampleModalLongTitle">Choose one of below methode to login</span>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <strong>Method 1 : </strong>
                            <span>Enter your wallet address</span>
                            <form className="mb-5">
                                <div className="form-group my-4 wallet-input">
                                    <input type="text" className="form-control" placeholder="wallet address"
                                           value={address} onChange={handleInputChange}/>
                                    <div className="input-group-append">
                                        <button className="btn btn-primary" type="button"
                                                onClick={handleSubmit}>Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <strong className="mt-5">Method 2 : </strong>
                            <span>login via MetaMask</span>
                            <div className="mt-5 d-flex justify-content-center">
                                <button onClick={handleMetaMask}
                                        data-dismiss="modal"
                                        className="btn btn-primary d-flex align-items-center justify-content-center">
                                    <img className="mr-2" width={35} height={35} src='/images/metamask-logo.png'
                                         alt="metamask"/>
                                    CONNECT METAMASK
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    )
}
