import React from 'react';

export default function HomeFooter() {
    return (
        <footer className="nk-footer">
            <section className="section py-0">
                <div className="container">
                    <div className="nk-block">
                        <div className="bg-grad-alt round subscribe-wrap tc-light animated" data-animate="fadeInUp"
                             data-delay=".2">
                            <div
                                className="row text-center text-md-left justify-content-center align-items-center gutter-vr-25px">
                                <div className="col-lg-6">
                                    <div className="wide-auto-sm">
                                        <h4 className="title-sm">Don't miss out, Stay updated</h4>
                                        <p>Sign up for updates and market news. Subscribe to our newsletter and receive
                                            update about ICOs and crypto tips</p>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="gap-3x d-none d-lg-block"></div>
                                    <form action="form/subscribe.php" className="nk-form-submit" method="post">
                                        <div
                                            className="field-inline field-inline-s2 field-inline-s2-sm bg-white shadow-soft">
                                            <div className="field-wrap">
                                                <input className="input-solid input-solid-md required email" type="text"
                                                       name="contact-email" placeholder="Enter your email"/>
                                                <input type="text" className="d-none" name="form-anti-honeypot"
                                                       value=""/>
                                            </div>
                                            <div className="submit-wrap">
                                                <button className="btn btn-md btn-secondary">Subscribe</button>
                                            </div>
                                        </div>
                                        <div className="form-results"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="nk-ovm ovm-top ovm-h-50 bg-white bdb ov-h"></div>
            </section>
            <div className="section section-footer section-m bg-transparent">
                <div className="container">
                    <div className="nk-block block-footer">
                        <div className="row">
                            <div className="col">
                                <div className="wgs wgs-text text-center mb-3">
                                    <ul className="social pdb-l justify-content-center">
                                        <li><a href="#"><em className="social-icon fab fa-facebook-f"></em></a></li>
                                        <li><a href="#"><em className="social-icon fab fa-twitter"></em></a></li>
                                        <li><a href="#"><em className="social-icon fab fa-youtube"></em></a></li>
                                        <li><a href="#"><em className="social-icon fab fa-github"></em></a></li>
                                        <li><a href="#"><em className="social-icon fab fa-bitcoin"></em></a></li>
                                        <li><a href="#"><em className="social-icon fab fa-medium-m"></em></a></li>
                                    </ul>
                                    <a href="./" className="footer-logo">
                                        <img src="images/logo.png" srcSet="images/logo2x.png 2x" alt="logo"/>
                                    </a>
                                    <div className="copyright-text copyright-text-s3 pdt-m">
                                        <p><span className="d-sm-block">Copyright &copy; 2018, ICO Crypto. Template Made By <a
                                            href="./">Softnio</a> &amp; Handcrafted by iO. </span>All trademarks and
                                            copyrights belong to their respective owners.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="nk-ovm shape-s-sm shape-center-bottom ov-h"></div>
            </div>
        </footer>
    )
}
