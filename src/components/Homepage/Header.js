import React, {useState} from 'react';
import {SHA3} from "sha3";
import {checkUserStatus} from "../../helper";

export default function HomeHeader() {
    const [address, setAddress] = useState('')

    const isAddress = (address) => {
        if (!/^(0x)?[0-9a-f]{40}$/i.test(address)) {
            // check if it has the basic requirements of an address
            return false;
        } else if (/^(0x)?[0-9a-f]{40}$/.test(address) || /^(0x)?[0-9A-F]{40}$/.test(address)) {
            // If it's all small caps or all all caps, return true
            return true;
        } else {
            // Otherwise check each case
            return isChecksumAddress(address);
        }
    };

    const isChecksumAddress = (address) => {
        // Check each case
        address = address.replace('0x', '');
        const hash = new SHA3();
        const addressHash = hash.update(address.toLowerCase());
        for (let i = 0; i < 40; i++) {
            // the nth letter should be uppercase if the nth digit of casemap is 1
            if ((parseInt(addressHash[i], 16) > 7 && address[i].toUpperCase() !== address[i]) || (parseInt(addressHash[i], 16) <= 7 && address[i].toLowerCase() !== address[i])) {
                return false;
            }
        }
        return true;
    };

    const handleSubmit = async () => {
        if (!address.length) {
            alert('Please enter a valid ETH address');
            return;
        } else {
            if (!isAddress(address)) {
                alert('Your wallet address is not valid');
                return;
            }
        }
        localStorage.setItem('address', address);
        const accounts = await window.ethereum.request({method: 'eth_requestAccounts'});
        await checkUserStatus(accounts[0]).then(res => {
            console.log(res);
            window.location.replace('/dashboard');
        }).catch(err => {
            console.log(err);
            alert('To login Dashboard you should invest first');
        })
    }

    const handleInputChange = (e) => {
        setAddress(e.target.value);
    }

    const handleMetaMask = async () => {
        localStorage.setItem('address', 'metaMask');
        const accounts = await window.ethereum.request({method: 'eth_requestAccounts'});
        await checkUserStatus(accounts[0]).then(res => {
            console.log(res);
            window.location.replace('/dashboard');
        }).catch(err => {
            console.log(err);
            alert('To login Dashboard you should invest first');
        })
    }

    return (
        <>
            <header className="nk-header page-header is-sticky is-shrink is-transparent is-light" id="header">
                <div className="header-main">
                    <div className="header-container container">
                        <div className="header-wrap">
                            <div className="header-logo logo animated" data-animate="fadeInDown" data-delay=".65">
                                <a href="./" className="logo-link">
                                    <img className="logo-dark" src="images/logo.png" srcSet="images/logo2x.png 2x"
                                         alt="logo"/>
                                    <img className="logo-light" src="images/logo.png" srcSet="images/logo2x.png 2x"
                                         alt="logo"/>
                                </a>
                            </div>
                            <div className="header-nav-toggle">
                                <a href="#" className="navbar-toggle" data-menu-toggle="header-menu">
                                    <div className="toggle-line">
                                        <span></span>
                                    </div>
                                </a>
                            </div>
                            <div className="header-navbar header-navbar-s1">
                                <nav className="header-menu" id="header-menu">
                                    <ul className="menu animated" data-animate="fadeInDown" data-delay=".75">
                                        <li className="menu-item"><a className="menu-link nav-link"
                                                                     href="#about">About</a>
                                        </li>
                                        <li className="menu-item"><a className="menu-link nav-link" href="#why">Why</a>
                                        </li>
                                        <li className="menu-item"><a className="menu-link nav-link"
                                                                     href="#benifits">Benifits</a></li>
                                        <li className="menu-item"><a className="menu-link nav-link" href="#token">Token
                                            Sale</a></li>
                                        <li className="menu-item"><a className="menu-link nav-link"
                                                                     href="#roadmap">Roadmap</a></li>
                                        <li className="menu-item"><a className="menu-link nav-link"
                                                                     href="#team">Team</a>
                                        </li>
                                        <li className="menu-item"><a className="menu-link nav-link"
                                                                     href="#faqs">Faqs</a>
                                        </li>
                                        <li className="menu-item"><a className="menu-link nav-link"
                                                                     href="#contact">Contact</a></li>
                                    </ul>
                                    <ul className="menu-btns menu-btns-s3 align-items-center animated"
                                        data-animate="fadeInDown" data-delay=".85">
                                        <li className="language-switcher language-switcher-s1 toggle-wrap">
                                            <a className="toggle-tigger" href="#">English</a>
                                            <ul className="toggle-class toggle-drop toggle-drop-left drop-list drop-list-sm">
                                                <li><a href="#">French</a></li>
                                                <li><a href="#">Chinese</a></li>
                                                <li><a href="#">Hindi</a></li>
                                            </ul>
                                        </li>

                                        <li>
                                            <button type="button" className="btn btn-primary" data-toggle="modal"
                                                    data-target="#login-modal">
                                                login
                                            </button>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="header-banner bg-light ov-h header-banner-jasmine">
                    <div className="background-shape"></div>
                    <div className="nk-banner">
                        <div className="banner banner-fs banner-single">
                            <div className="nk-block nk-block-header my-auto">
                                <div className="container">
                                    <div className="row align-items-center justify-content-center">
                                        <div className="col-lg-9 text-center">
                                            <div className="banner-caption">
                                                <div className="cpn-head">
                                                    <h1 className="title title-xl-2 animated" data-animate="fadeInUp"
                                                        data-delay="1.25">#1 Best Selling ICO Landing <br
                                                        className="d-none d-xl-block"/> template for ICO Startup Agency
                                                    </h1>
                                                </div>
                                                <div className="cpn-text cpn-text-s2">
                                                    <p className="lead-s2 animated" data-animate="fadeInUp"
                                                       data-delay="1.35">Most Trending, Clean and Elegant Design <br
                                                        className="d-none d-sm-block"/>based on deeply research</p>
                                                </div>
                                                <div className="cpn-action">
                                                    <ul className="cpn-links animated" data-animate="fadeInUp"
                                                        data-delay="1.45">
                                                        <li><a className="link link-primary" href="#"><em
                                                            className="link-icon icon-circle ti ti-files"></em> <span>White Paper</span></a>
                                                        </li>
                                                        <li><a className="link link-primary" href="#"><em
                                                            className="link-icon icon-circle ti ti-file"></em> <span>One Pager</span></a>
                                                        </li>
                                                        <li><a className="link link-primary video-play"
                                                               href="https://www.youtube.com/watch?v=SSo_EIwHSd4"><em
                                                            className="link-icon icon-circle ti ti-control-play"></em>
                                                            <span>How it works</span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="nk-block nk-block-status">
                                <div className="container">
                                    <div className="row justify-content-center">
                                        <div className="col-lg-8">
                                            <div className="row justify-content-center">
                                                <div className="col-md-8">
                                                    <div className="token-status token-status-s4">
                                                        <div className="token-box token-box-s2 bg-transparent animated"
                                                             data-animate="fadeInUp" data-delay="1.65">
                                                            <h4 className="title title-xs-alt tc-default">PRE SALE
                                                                STARTING
                                                                IN</h4>
                                                            <div className="countdown-s3 countdown-s4 countdown"
                                                                 data-date="2020/03/15"></div>
                                                        </div>
                                                        <div className="token-action token-action-s1 animated"
                                                             data-animate="fadeInUp" data-delay="1.65">
                                                            <a className="btn btn-rg btn-grad btn-grad-alt" href="#">Sign
                                                                UP &amp; Join our PRe-Sale list</a>
                                                        </div>
                                                        <ul className="icon-list animated" data-animate="fadeInUp"
                                                            data-delay="1.65">
                                                            <li><em className="fab fa-bitcoin"></em></li>
                                                            <li><em className="fas fa-won-sign"></em></li>
                                                            <li><em className="fab fa-cc-visa"></em></li>
                                                            <li><em className="fab fa-cc-mastercard"></em></li>
                                                        </ul>
                                                        <div className="circle-animation animated" data-animate="fadeIn"
                                                             data-delay="1.55">
                                                            <div className="circle-animation-l1 ca">
                                                            <span
                                                                className="circle-animation-l1-d1 ca-dot ca-color-1"></span>
                                                                <span
                                                                    className="circle-animation-l1-d2 ca-dot ca-color-2"></span>
                                                                <span
                                                                    className="circle-animation-l1-d3 ca-dot ca-color-3"></span>
                                                                <span
                                                                    className="circle-animation-l1-d4 ca-dot ca-color-1"></span>
                                                                <span
                                                                    className="circle-animation-l1-d5 ca-dot ca-color-2"></span>
                                                                <span
                                                                    className="circle-animation-l1-d6 ca-dot ca-color-3"></span>
                                                            </div>
                                                            <div className="circle-animation-l2 ca">
                                                            <span
                                                                className="circle-animation-l2-d1 ca-dot ca-color-1"></span>
                                                                <span
                                                                    className="circle-animation-l2-d2 ca-dot ca-color-3"></span>
                                                                <span
                                                                    className="circle-animation-l2-d3 ca-dot ca-color-2"></span>
                                                                <span
                                                                    className="circle-animation-l2-d4 ca-dot ca-color-1"></span>
                                                                <span
                                                                    className="circle-animation-l2-d5 ca-dot ca-color-2"></span>
                                                            </div>
                                                            <div className="circle-animation-l3 ca">
                                                            <span
                                                                className="circle-animation-l3-d1 ca-dot ca-color-1"></span>
                                                                <span
                                                                    className="circle-animation-l3-d2 ca-dot ca-color-3"></span>
                                                                <span
                                                                    className="circle-animation-l3-d3 ca-dot ca-color-2"></span>
                                                                <span
                                                                    className="circle-animation-l3-d4 ca-dot ca-color-1"></span>
                                                                <span
                                                                    className="circle-animation-l3-d5 ca-dot ca-color-2"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="particles-bg" className="particles-container particles-bg" data-pt-base="#00c0fa"
                         data-pt-base-op=".3" data-pt-line="#2b56f5" data-pt-line-op=".5" data-pt-shape="#00c0fa"
                         data-pt-shape-op=".2"></div>
                </div>
            </header>
            <div className="modal fade" id="login-modal" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div className="modal-content bg-white">
                        <div className="modal-header">
                            <span className="modal-title" id="exampleModalLongTitle">Choose one of below methode to login</span>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <strong>Method 1 : </strong>
                            <span>Enter your wallet address</span>
                            <form className="mb-5">
                                <div className="form-group my-4 wallet-input">
                                    <input type="text" className="form-control" placeholder="wallet address"
                                           value={address} onChange={handleInputChange}/>
                                    <div className="input-group-append">
                                        <button className="btn btn-primary" type="button"
                                                onClick={handleSubmit}>Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <strong className="mt-5">Method 2 : </strong>
                            <span>login via MetaMask</span>
                            <div className="mt-5 d-flex justify-content-center">
                                <button onClick={handleMetaMask}
                                        className="btn btn-primary d-flex align-items-center justify-content-center">
                                    <img className="mr-2" width={35} height={35} src='/images/metamask-logo.png'
                                         alt="metamask"/>
                                    CONNECT METAMASK
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
