import React, {useEffect, useState} from 'react';
import HomeHeader from "./components/Homepage/Header";
import HomeFooter from "./components/Homepage/Footer";
import DashboardHeader from "./components/Dashboard/Header";
import Homepage from "./components/Homepage/Hompage";
import Dashboard from "./components/Dashboard/Dashboard";
import {getUserInfo, getRefAddress, checkUserStatus} from "./helper";

const Web3 = require('web3');

const HomeLayout = () => {
    const [userData, setUserData] = useState(
        {
            connected: false,
            refId: '',
            address: '',
            balance: '',
            totalWithdraw: '',
            lastWithdrawal: '',
            investTime: '',
            numReferral: '',
            totalRefPayment: '',
            planId: '',
            claim: '',
        }
    )

    useEffect(async () => {
        let web3 = new Web3(Web3.givenProvider);
        const address = localStorage.getItem("address");
        if (address) {
            if (address === 'metaMask') {
                const accounts = await window.ethereum.request({method: 'eth_requestAccounts'});
                if (userData.totalWithdraw === '') {
                    getUserInfo(accounts[0]).then(async res => {
                        console.log(res);
                        const balance = await web3.eth.getBalance(accounts[0]).then(balance => {
                            return parseFloat(web3.utils.fromWei(balance)).toFixed(4);
                        })

                        if (typeof res[0] !== "undefined")
                            setUserData(
                                {
                                    ...userData,
                                    address: accounts[0],
                                    totalWithdraw: res[0],
                                    lastWithdrawal: res[1],
                                    investTime: res[2],
                                    numReferral: res[3],
                                    totalRefPayment: res[4],
                                    planId: res[5],
                                    refId: res[6],
                                    balance: balance,
                                    // claim: claimValue,
                                }
                            )
                    });
                }
            } else {
                if (userData.totalWithdraw === '')
                    getUserInfo(address).then(async res => {
                        const balance = await web3.eth.getBalance(address).then(balance => {
                            return parseFloat(web3.utils.fromWei(balance)).toFixed(4);
                        })

                        if (typeof res[0] !== "undefined")
                            setUserData(
                                {
                                    ...userData,
                                    connected: true,
                                    address: address,
                                    totalWithdraw: res[0],
                                    lastWithdrawal: res[1],
                                    investTime: res[2],
                                    numReferral: res[3],
                                    totalRefPayment: res[4],
                                    planId: res[5],
                                    refId: res[6],
                                    balance: balance,
                                    // claim: claimValue,
                                }
                            )
                    });
            }
        }
    }, [])

    return (
        <div>
            <HomeHeader/>
            <Homepage userData={userData}/>
            <HomeFooter/>
        </div>
    )
}

const DashboardLayout = () => {
    const [userData, setUserData] = useState(
        {
            refId: '',
            refAddress: '',
            address: '',
            balance: '',
            totalWithdraw: '',
            lastWithdrawal: '',
            investTime: '',
            numReferral: '',
            totalRefPayment: '',
            planId: '',
            claim: '',
        }
    )
    useEffect(async () => {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const ref = urlParams.get('ref');
        if (ref) {
            await getRefAddress(ref).then(res => {
                setUserData({
                    ...userData,
                    refAddress: res,
                })
            })
        }
        let web3 = new Web3(Web3.givenProvider);
        const address = localStorage.getItem("address");
        if (address === 'metaMask') {
            const accounts = await window.ethereum.request({method: 'eth_requestAccounts'});
            await checkUserStatus(accounts[0]).then(res => {
                if (userData.totalWithdraw === '') {
                    getUserInfo(accounts[0]).then(async res => {
                        console.log(res);
                        const balance = await web3.eth.getBalance(accounts[0]).then(balance => {
                            return parseFloat(web3.utils.fromWei(balance)).toFixed(4);
                        })
                        if (typeof res[0] !== "undefined") {
                            setUserData(
                                {
                                    ...userData,
                                    address: accounts[0],
                                    totalWithdraw: res[0],
                                    lastWithdrawal: res[1],
                                    investTime: res[2],
                                    numReferral: res[3],
                                    totalRefPayment: res[4],
                                    planId: res[5],
                                    refId: res[6],
                                    balance: balance,
                                    // claim: claimValue,
                                }
                            )
                        }
                    });
                }
            }).catch(err => {
                console.log(err)
                alert('To login Dashboard you should invest first');
                window.location.replace('/');
            })
        } else {
            await checkUserStatus(address).then(res => {
                if (userData.totalWithdraw === '')
                    getUserInfo(address).then(async res => {
                        const balance = await web3.eth.getBalance(address).then(balance => {
                            return parseFloat(web3.utils.fromWei(balance)).toFixed(4);
                        })
                        if (typeof res[0] !== "undefined")
                            setUserData(
                                {
                                    ...userData,
                                    address: address,
                                    totalWithdraw: res[0],
                                    lastWithdrawal: res[1],
                                    investTime: res[2],
                                    numReferral: res[3],
                                    totalRefPayment: res[4],
                                    planId: res[5],
                                    refId: res[6],
                                    balance: balance,
                                    // claim: claimValue,
                                }
                            )
                    });
            }).catch(err => {
                console.log(err)
                alert('To login Dashboard you should invest first');
                window.location.replace('/');
            })
        }
    }, [])

    return (
        <div>
            <DashboardHeader userData={userData}/>
            <Dashboard userData={userData}/>
        </div>
    )
}

export {HomeLayout, DashboardLayout};
